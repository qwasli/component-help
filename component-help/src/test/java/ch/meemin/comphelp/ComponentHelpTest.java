package ch.meemin.comphelp;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Assert;
import org.junit.Test;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;

import ch.meemin.comphelp.HelpChange.HelpChangeNotifier;
import ch.meemin.comphelp.HelpChange.HelpChanger;

// JUnit tests here
public class ComponentHelpTest {

	@Test
	public void simpleHelpCunstructorWithNull() {
		SimpleHelp sh = new SimpleHelp(null, (AbstractComponent) null);
		Assert.assertNotNull(sh);
	}

	@Test
	public void header() {
		Label l = SimpleHelp.header("blub", "style");
		Assert.assertNotNull(l);
		Assert.assertEquals("blub", l.getValue());
		Assert.assertEquals("style", l.getStyleName());
	}

	@Test
	public void headerNullStyle() {
		Label l = SimpleHelp.header("blub", null);
		Assert.assertNotNull(l);
		Assert.assertEquals("blub", l.getValue());
		Assert.assertEquals("", l.getStyleName());
	}

	@Test
	public void headerAndHtml() {
		ArrayList<AbstractComponent> cs = SimpleHelp.headerAndHtml(x -> ((Boolean) x) ? "JA" : "NEIN", Boolean.TRUE, null,
				Boolean.FALSE);
		Assert.assertEquals(2, cs.size());
		Assert.assertEquals("JA", ((Label) cs.get(0)).getValue());
		Assert.assertEquals("NEIN", ((Label) cs.get(1)).getValue());
		Assert.assertEquals(ContentMode.HTML, ((Label) cs.get(1)).getContentMode());
	}

	@Test
	public void createCopy() {
		Button b = new Button("captain", VaadinIcons.VAADIN_H);
		b.setHeight("1px");
		b.setWidth("2px");
		b.setDescription("desc");
		b.setStyleName("these are four styles");
		AbstractComponent c = SimpleHelp.createCopy(b);
		Assert.assertTrue(c instanceof Button);
		Assert.assertEquals(b.getCaption(), c.getCaption());
		Assert.assertEquals(b.getStyleName(), c.getStyleName());
		Assert.assertEquals(b.getHeight(), c.getHeight(), 0);
		Assert.assertEquals(b.getWidth(), c.getWidth(), 0);
		Assert.assertEquals(b.getWidthUnits(), c.getWidthUnits());
		Assert.assertEquals(b.getHeightUnits(), c.getHeightUnits());
		Assert.assertEquals(b.getDescription(), c.getDescription());
		Assert.assertEquals(b.getIcon(), c.getIcon());

	}

	@Test
	public void HelpParagraph() {
		HH hh = new HH();
		HelpParagraph hp = new HelpParagraph(hh);
		Assert.assertEquals(2, hp.getComponentCount());
		hh.setAb(true);
		Assert.assertEquals(3, hp.getComponentCount());
		hh.setAb(false);
		Assert.assertEquals(2, hp.getComponentCount());
	}

	private class HH extends HelpChanger implements HasHelp {
		AtomicBoolean ab = new AtomicBoolean(false);

		public void setAb(boolean sab) {
			if (ab.get() == sab)
				return;
			this.ab.set(sab);
			fireHelpChanged();
		}

		@Override
		public List<HasHelp> getSubHelp() {
			if (!ab.get())
				return null;
			else
				return new SubHelp(new HH());
		}

		@Override
		public AbstractComponent getHelpTarget() {
			return null;
		}

		@Override
		public List<AbstractComponent> getHelp() {
			return SimpleHelp.headerAndHtml("head", null, "test");
		}

	}

}
