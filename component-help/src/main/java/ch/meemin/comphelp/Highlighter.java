package ch.meemin.comphelp;

import ch.meemin.comphelp.client.HighlighterState;

import com.vaadin.server.AbstractClientConnector;
import com.vaadin.server.AbstractExtension;
import com.vaadin.ui.Component;

public class Highlighter extends AbstractExtension {

	public Component getComponent() {
		return (Component) getState().component;
	}

	public void setComponent(Component component) {
		getState().component = component;
	}

	// We must override getState() to cast the state to ComponentHelpState
	@Override
	public HighlighterState getState() {
		return (HighlighterState) super.getState();
	}

	@Override
	public void extend(AbstractClientConnector target) {
		super.extend(target);
	}
}
