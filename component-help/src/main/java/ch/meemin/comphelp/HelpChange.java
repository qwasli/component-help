package ch.meemin.comphelp;

import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

public class HelpChange {
	/**
	 * Class that can be used to handle help change events.
	 * 
	 * @author raffael
	 *
	 */
	public static class HelpChanger implements HelpChangeNotifier {

		WeakHashMap<HelpChangeListener, Boolean> listeners;
		private WeakReference<HasHelp> hh;
		
		public HelpChanger() {
			if(this instanceof HasHelp)
				this.hh = new WeakReference<HasHelp>((HasHelp)this);
			else
				throw new IllegalStateException();
		}

		public HelpChanger(HasHelp hh) {
			this.hh = new WeakReference<HasHelp>(hh);
		}

		public void fireHelpChanged() {
			if (listeners == null)
				return;
			HasHelp hh = this.hh.get();
			if(hh ==null)
				return;
			for (HelpChangeListener hcl : listeners.keySet()) {
				if (hcl != null)
					hcl.helpChanged(hh);
			}
		}

		@Override
		public void addHelpChangeListener(HelpChangeListener hcl) {
			if (listeners == null)
				listeners = new WeakHashMap<HelpChange.HelpChangeListener, Boolean>();
			if (!listeners.containsKey(hcl))
				listeners.put(hcl, Boolean.FALSE);
		}

		@Override
		public void removeHelpChangeListener(HelpChangeListener hcl) {
			if (listeners == null)
				return;
			listeners.remove(hcl);
		}

		@Override
		public void removeAllHelpChangeListener() {
			if (listeners == null)
				return;
			listeners.clear();
		}
	}

	public interface HelpChangeNotifier {
		public void addHelpChangeListener(HelpChangeListener hcl);

		public void removeHelpChangeListener(HelpChangeListener hcl);

		public void removeAllHelpChangeListener();
	}

	/**
	 * Interface to mark that it has a {@link HelpChangeNotifier}. This is used by {@link HelpParagraph}.
	 * 
	 * @author raffael
	 *
	 */
	public interface HasHelpChangeNotifier {
		public HelpChangeNotifier getHelpChangeNotifier();
	}

	public interface HelpChangeListener {
		public void helpChanged(HasHelp hh);
	}

}
