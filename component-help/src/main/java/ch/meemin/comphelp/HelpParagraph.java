package ch.meemin.comphelp;

import java.util.List;

import ch.meemin.comphelp.HelpChange.HasHelpChangeNotifier;
import ch.meemin.comphelp.HelpChange.HelpChangeListener;
import ch.meemin.comphelp.HelpChange.HelpChangeNotifier;

import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Layout;

/**
 * Simple css layout to display a {@link HasHelp}Tree. If the given HasHelp is instance of {@link HelpChangeNotifier} or
 * {@link HasHelpChangeNotifier} it will register itself to that and rebuild if the help changes. It does not register
 * to Notifiers further down in the tree
 * 
 * @author raffael
 *
 */
public class HelpParagraph extends CssLayout implements HelpChangeListener {
	public static final String STYLE = "helpparagraph";

	public HelpParagraph(HasHelp hh) {
		setStyleName(STYLE);
		build(hh);
		if (hh instanceof HelpChangeNotifier) {
			((HelpChangeNotifier) hh).addHelpChangeListener(this);
		} else if (hh instanceof HasHelpChangeNotifier) {
			((HasHelpChangeNotifier) hh).getHelpChangeNotifier().addHelpChangeListener(this);
		}
	}

	private void build(HasHelp hh) {
		addHelpToLayout(hh, this);

		final List<HasHelp> subHelps = hh.getSubHelp();
		if (subHelps != null && !subHelps.isEmpty()) {
			if (subHelps instanceof SubHelp)
				addStyleName(((SubHelp) subHelps).getStyle());
			for (HasHelp shh : subHelps) {
				addComponent(new HelpParagraph(shh));
			}
		}
	}

	private static void addHelpToLayout(HasHelp hh, Layout mainLayout) {
		AbstractComponent targetComponent = hh.getHelpTarget();
		List<AbstractComponent> helps = hh.getHelp();
		if (helps != null) {
			for (AbstractComponent help : helps) {
				if (targetComponent != null) {
					Highlighter hl = new Highlighter();
					hl.setComponent(targetComponent);
					hl.extend(help);
				}
				mainLayout.addComponent(help);
			}
		}
	}

	@Override
	public void helpChanged(HasHelp hh) {
		removeAllComponents();
		build(hh);
	}

}