package ch.meemin.comphelp.client;

import ch.meemin.comphelp.Highlighter;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.client.ComponentConnector;
import com.vaadin.client.ServerConnector;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.client.extensions.AbstractExtensionConnector;
import com.vaadin.client.ui.VOverlay;
import com.vaadin.client.ui.VWindow;
import com.vaadin.shared.ui.Connect;

// Connector binds client-side widget class to server-side component class
// Connector lives in the client and the @Connect annotation specifies the
// corresponding server-side component
@Connect(Highlighter.class)
public class HighlighterConnector extends AbstractExtensionConnector implements MouseOverHandler, MouseOutHandler {

	Element highlight = null;

	public HighlighterConnector() {

	}

	// We must implement getState() to cast to correct type
	@Override
	public HighlighterState getState() {
		return (HighlighterState) super.getState();
	}

	public ComponentConnector getComponent() {
		return (ComponentConnector) getState().component;
	}

	// Whenever the state changes in the server-side, this method is called
	@Override
	public void onStateChanged(StateChangeEvent stateChangeEvent) {
		super.onStateChanged(stateChangeEvent);
	}

	@Override
	protected void extend(ServerConnector target) {
		final Widget widget = ((ComponentConnector) target).getWidget();

		widget.addHandler(this, MouseOverEvent.getType());
		widget.addHandler(this, MouseOutEvent.getType());
	}

	@Override
	public void onMouseOver(MouseOverEvent event) {
		if (getComponent() == null || getComponent().getWidget() == null || getComponent().getWidget().getElement() == null)
			return;
		Element element = getComponent().getWidget().getElement();
		int aL = element.getAbsoluteLeft();
		int aT = element.getAbsoluteTop();
		int width = element.getOffsetWidth();
		int height = element.getOffsetHeight();
		boolean visible = UIObject.isVisible(element) && (width > 0) && (height > 0);
		if (!visible)
			return;
		highlight = DOM.createDiv();
		Style style = highlight.getStyle();
		highlight.addClassName("component-help-highlight");
		style.setTop(aT, Unit.PX);
		style.setLeft(aL, Unit.PX);
		style.setWidth(width, Unit.PX);
		style.setHeight(height, Unit.PX);

		VOverlay.getOverlayContainer(getConnection()).appendChild(highlight);

		style.setPosition(Position.ABSOLUTE);
		style.setZIndex(VWindow.Z_INDEX + 2000);
	}

	@Override
	public void onMouseOut(MouseOutEvent event) {
		if (highlight == null)
			return;
		highlight.removeFromParent();
		highlight = null;
	}

	@Override
	public void onUnregister() {
		super.onUnregister();
		onMouseOut(null);
	}

}
