package ch.meemin.comphelp;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.themes.ValoTheme;

public class CompList extends ArrayList<AbstractComponent> {

	public CompList() {
		super();
	}

	public CompList(AbstractComponent... components) {
		super(components.length);
		for (AbstractComponent h : components) {
			add(h);
		}
	}

	public CompList append(AbstractComponent component) {
		this.add(component);
		return this;
	}

	public CompList a(AbstractComponent component) {
		return append(component);
	}

	public CompList append(String text) {
		this.add(SimpleHelp.label(text));
		return this;
	}

	public CompList a(String text) {
		return append(text);
	}

	public CompList append(String text, String style) {
		this.add(SimpleHelp.header(text, style));
		return this;
	}

	public CompList a(String text, String style) {
		return append(text, style);
	}

	public CompList appendH1(String text) {
		this.add(SimpleHelp.header(text, ValoTheme.LABEL_H1));
		return this;
	}

	public CompList aH1(String text) {
		return appendH1(text);
	}

	public CompList appendH2(String text) {
		this.add(SimpleHelp.header(text, ValoTheme.LABEL_H2));
		return this;
	}

	public CompList aH2(String text) {
		return appendH2(text);
	}

	public CompList appendH3(String text) {
		this.add(SimpleHelp.header(text, ValoTheme.LABEL_H3));
		return this;
	}

	public CompList aH3(String text) {
		return appendH3(text);
	}

	public CompList appendH4(String text) {
		this.add(SimpleHelp.header(text, ValoTheme.LABEL_H4));
		return this;
	}

	public CompList aH4(String text) {
		return appendH4(text);
	}

	public CompList appendAll(List<AbstractComponent> components) {
		if (components != null)
			addAll(components);
		return this;
	}

	public CompList all(List<AbstractComponent> components) {
		return appendAll(components);
	}

	public CompList appendCopy(AbstractComponent ac) {
		return append(SimpleHelp.createCopy(ac));
	}

	public CompList copy(AbstractComponent ac) {
		return appendCopy(ac);
	}

	public CompList appendHTML(String html) {
		Label l = SimpleHelp.label(html);
		l.setContentMode(ContentMode.HTML);
		this.add(l);
		return this;
	}

	public CompList html(String html) {
		return appendHTML(html);
	}

}
