package ch.meemin.comphelp;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.ui.AbstractComponent;

/**
 * Helper class to create a List of {@link HasHelp} objects. This list could be used in the getSubHelp() function. This
 * class is just an Implementation of Arraylist<HasHelp> and adds some append functions.
 * 
 * @author Raffael Bachmann
 *
 */
public class SubHelp extends ArrayList<HasHelp> {
	private static final long serialVersionUID = 6713484316595544082L;

	private String style;

	public SubHelp() {
		super();
	}

	public SubHelp(HasHelp... hh) {
		super(hh.length);
		for (HasHelp h : hh) {
			add(h);
		}
	}

	public SubHelp append(HasHelp hh) {
		this.add(hh);
		return this;
	}

	public SubHelp appendLabel(AbstractComponent target, String l) {
		this.add(new SimpleHelp(target, false, SimpleHelp.label(l)));
		return this;
	}

	public SubHelp label(AbstractComponent target, String l) {
		return appendLabel(target, l);
	}

	public SubHelp appendHtml(AbstractComponent target, String html) {
		this.add(new SimpleHelp(target, false, SimpleHelp.html(html)));
		return this;
	}

	public SubHelp html(AbstractComponent target, String html) {
		return appendHtml(target, html);
	}

	public SubHelp a(HasHelp hh) {
		return append(hh);
	}

	public SubHelp appendAll(List<HasHelp> hhs) {
		if (hhs != null)
			addAll(hhs);
		return this;
	}

	public SubHelp aa(List<HasHelp> hhs) {
		return appendAll(hhs);
	}

	public SubHelp appendCopy(AbstractComponent target, String expl) {
		if (expl != null)
			append(new SimpleHelp(target, true, SimpleHelp.label(expl)));
		else
			append(new SimpleHelp(target, true));
		return this;
	}

	public SubHelp appendCopyHtml(AbstractComponent target, String html) {
		if (html != null)
			append(new SimpleHelp(target, true, SimpleHelp.html(html)));
		else
			append(new SimpleHelp(target, true));
		return this;
	}

	public SubHelp ach(AbstractComponent target, String expl) {
		return appendCopyHtml(target, expl);
	}

	public SubHelp ac(AbstractComponent target, String expl) {
		return appendCopy(target, expl);
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}
}
