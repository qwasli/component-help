package ch.meemin.comphelp;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Label;

public class SimpleHelp implements HasHelp {

	private AbstractComponent helpTarget;
	private List<AbstractComponent> helps;

	/**
	 * 
	 * Creates a Simlehelp having a help target and uses the list of {@link AbstractComponent} as help information
	 * 
	 * @param target
	 * @param helps
	 */
	public SimpleHelp(AbstractComponent target, List<AbstractComponent> helps) {
		this.helpTarget = target;
		if (helps != null)
			this.helps = helps;
		else {
			this.helps = new ArrayList<>();
			AbstractComponent c = createCopy(target);
			if (c != null)
				this.helps.add(c);
		}
		for (AbstractComponent help : this.helps) {
			help.addStyleName(HasHelp.HASTARGETSTYLE);
		}
	}

	public SimpleHelp(AbstractComponent target, AbstractComponent... help) {
		this.helpTarget = target;
		this.helps = new ArrayList<>();
		if (help != null) {
			for (AbstractComponent h : help)
				if (h != null)
					this.helps.add(h);
		} else {
			AbstractComponent c = createCopy(target);
			if (c != null)
				this.helps.add(c);
		}

		for (AbstractComponent h : this.helps)
			h.addStyleName(HasHelp.HASTARGETSTYLE);
	}

	public SimpleHelp(AbstractComponent target, boolean copy, AbstractComponent... help) {
		this.helpTarget = target;
		this.helps = new ArrayList<>();
		if (copy) {
			AbstractComponent c = createCopy(target);
			if (c != null)
				this.helps.add(c);
		}
		if (help != null) {
			for (AbstractComponent h : help)
				if (h != null)
					this.helps.add(h);
		}
		for (AbstractComponent h : this.helps)
			h.addStyleName(HasHelp.HASTARGETSTYLE);
	}

	public SimpleHelp(List<AbstractComponent> helps) {
		this.helps = helps;
	}

	public SimpleHelp(AbstractComponent help) {
		this.helps = new ArrayList<>();
		this.helps.add(help);
	}

	@Override
	public AbstractComponent getHelpTarget() {
		return helpTarget;
	}

	@Override
	public List<AbstractComponent> getHelp() {
		return helps;
	}

	@Override
	public List<HasHelp> getSubHelp() {
		return null;
	}

	public static AbstractComponent createCopy(AbstractComponent target) {
		AbstractComponent help;
		try {
			help = target.getClass().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			return null;
		}
		help.setHeight(target.getHeight(), target.getHeightUnits());
		help.setWidth(target.getWidth(), target.getWidthUnits());
		help.setCaption(target.getCaption());
		help.setDescription(target.getDescription());
		help.setStyleName(target.getStyleName());
		help.setIcon(target.getIcon());
		return help;
	}

	public static Label header(String text, String style) {
		Label l = new Label(text);
//		if (style != null && !style.isEmpty())
			l.setStyleName(style);
		return l;
	}

	public static ArrayList<AbstractComponent> headerAndText(String header, String style, String text) {
		ArrayList<AbstractComponent> list = new ArrayList<>();
		list.add(header(header, style));
		list.add(label(text));
		return list;
	}

	public static ArrayList<AbstractComponent> headerAndHtml(String header, String style, String text) {
		ArrayList<AbstractComponent> list = new ArrayList<>();
		list.add(header(header, style));
		list.add(html(text));
		return list;
	}

	public static ArrayList<AbstractComponent> headerAndText(StringGenerator sg, Object header, String style,
			Object text) {
		ArrayList<AbstractComponent> list = new ArrayList<>();
		list.add(header(sg.get(header), style));
		list.add(label(sg.get(text)));
		return list;
	}

	public static ArrayList<AbstractComponent> headerAndHtml(StringGenerator sg, Object header, String style,
			Object text) {
		ArrayList<AbstractComponent> list = new ArrayList<>();
		list.add(header(sg.get(header), style));
		list.add(html(sg.get(text)));
		return list;
	}

	public static Label label(String text) {
		Label l = new Label(text);
		l.setSizeUndefined();
		return l;
	}

	public static Label html(String text) {
		Label l = new Label(text);
		l.setSizeUndefined();
		l.setContentMode(ContentMode.HTML);
		return l;
	}

	@FunctionalInterface
	public interface StringGenerator {
		public String get(Object o);
	}

}
