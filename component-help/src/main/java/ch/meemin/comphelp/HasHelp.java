package ch.meemin.comphelp;

import java.util.List;

import com.vaadin.ui.AbstractComponent;

public interface HasHelp {

	public static final String HASTARGETSTYLE = "hastarget";

	/**
	 * Returns a component representing the help. This can be anything, but mostly it is something containing Text. For
	 * example a Label. When the mouse is over the returned Component, the targetComponent, if not null, will be
	 * highlighted.
	 * 
	 * @return
	 */
	public List<AbstractComponent> getHelp();

	/**
	 * Returns a component representing the help. This can be anything, but mostly it is something containing Text. For
	 * example a Label. When the mouse is over the returned Component, the targetComponent, if not null, will be
	 * highlighted.
	 * 
	 * @return
	 */
	public List<HasHelp> getSubHelp();

	/**
	 * Returns the component the help is about. May be null.
	 * 
	 * @return
	 */
	public AbstractComponent getHelpTarget();
}
