package ch.meemin.comphelp.demo;

import java.util.List;
import java.util.Random;

import ch.meemin.comphelp.CompList;
import ch.meemin.comphelp.HasHelp;
import ch.meemin.comphelp.HelpChange.HasHelpChangeNotifier;
import ch.meemin.comphelp.HelpChange.HelpChangeNotifier;
import ch.meemin.comphelp.HelpChange.HelpChanger;
import ch.meemin.comphelp.SubHelp;

import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class MyLayout extends HorizontalLayout implements HasHelp, HasHelpChangeNotifier, ClickListener {

	private HelpChanger hcn;

	private Random rand = new Random();

	public MyLayout() {
		addComponent(new MyButton(true, this));
		addComponent(new MyButton(false, this));
	}

	@Override
	public List<AbstractComponent> getHelp() {
		return new CompList().append("These are the buttons", ValoTheme.LABEL_H2);
	}

	@Override
	public List<HasHelp> getSubHelp() {
		final SubHelp sh = new SubHelp();
		for (Component c : this) {
			if (c instanceof HasHelp)
				sh.add((HasHelp) c);
		}
		return sh;
	}

	@Override
	public AbstractComponent getHelpTarget() {
		return null;
	}

	@Override
	public HelpChangeNotifier getHelpChangeNotifier() {
		if (this.hcn == null)
			this.hcn = new HelpChanger(this);
		return this.hcn;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		addComponent(new MyButton(rand.nextBoolean(), this));
		if (hcn != null)
			hcn.fireHelpChanged();
	}
}
