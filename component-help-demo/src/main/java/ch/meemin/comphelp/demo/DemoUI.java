package ch.meemin.comphelp.demo;

import java.util.List;

import javax.servlet.annotation.WebServlet;

import ch.meemin.comphelp.CompList;
import ch.meemin.comphelp.HasHelp;
import ch.meemin.comphelp.HelpParagraph;
import ch.meemin.comphelp.SubHelp;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@Theme("demo")
@Title("ComponentHelp Add-on Demo")
@SuppressWarnings("serial")
public class DemoUI extends UI implements HasHelp {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = DemoUI.class, widgetset = "ch.meemin.comphelp.demo.DemoWidgetSet")
	public static class Servlet extends VaadinServlet {}

	MyLayout myLayout = new MyLayout();

	CssLayout help;

	@Override
	protected void init(VaadinRequest request) {

		// Show it in the middle of the screen
		final VerticalLayout layout = new VerticalLayout();
		setContent(layout);

		layout.addComponent(myLayout);

		layout.addComponent(new Button("Show Help", new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				HelpParagraph h = new HelpParagraph(DemoUI.this);
				layout.replaceComponent(help, h);
				help = h;
			}
		}));
	}

	@Override
	public List<AbstractComponent> getHelp() {
		return new CompList().append("This is the Help", ValoTheme.LABEL_H1);
	}

	@Override
	public List<HasHelp> getSubHelp() {
		return new SubHelp(myLayout);
	}

	@Override
	public AbstractComponent getHelpTarget() {
		return null;
	}

}
