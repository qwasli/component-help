package ch.meemin.comphelp.demo;

import java.util.List;

import ch.meemin.comphelp.CompList;
import ch.meemin.comphelp.HasHelp;
import ch.meemin.comphelp.SimpleHelp;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Button;

public class MyButton extends Button implements HasHelp {

	private boolean state;

	public MyButton() {}

	public MyButton(boolean state, ClickListener cl) {
		this.state = state;
		if (state) {
			setIcon(VaadinIcons.CHECK);
		} else {
			setIcon(VaadinIcons.BUG);
		}
		addClickListener(cl);
	}

	@Override
	public List<AbstractComponent> getHelp() {
		if (state)
			return new CompList().append(SimpleHelp.createCopy(this)).append(
					"This button looks right. It does nothing, but has help text");
		return new CompList().appendCopy(this).appendHTML(
				"This button looks buggy (<span class='FontAwesome'></span>). It does nothing, but has help text");
	}

	@Override
	public List<HasHelp> getSubHelp() {
		return null;
	}

	@Override
	public AbstractComponent getHelpTarget() {
		return this;
	}

}
